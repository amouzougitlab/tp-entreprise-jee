/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.epl.entreprise.service;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import java.util.List;
import tg.univlome.epl.entreprise.api.entities.Employe;
import tg.univlome.epl.entreprise.api.services.EmployeServiceBeanLocal;

/**
 *
 * @author kevo
 */
@Stateless
public class EmployeServiceBean implements EmployeServiceBeanLocal{
    
     @PersistenceContext(name = "entreprisePU")
    private EntityManager em;

    @Override
    public void ajouter(Employe employe) {
        em.persist(employe);
    }

    @Override
    public void modifier(Employe employe) {
        em.merge(employe);
    }

    @Override
    public List<Employe> lister() {
        Query query = em.createQuery("SELECT e FROM Employe e ORDER BY e.nom, e.prenom");
        return query.getResultList();
    }

    @Override
    public void supprimer(int id) {
        Employe employe = em.find(Employe.class, id);
        if (employe != null) {
            em.remove(employe);
        }
    }

    @Override
    public Integer compter() {
        Query query = em.createQuery("SELECT COUNT(e) FROM Employe e");
        Long count = (Long) query.getSingleResult();
        return count.intValue();
    }

    @Override
    public Employe recuperer(int id) {
        return em.find(Employe.class, id);
    }
    
}
