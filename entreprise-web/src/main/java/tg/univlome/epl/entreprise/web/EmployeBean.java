/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.epl.entreprise.web;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Named;
import java.io.Serializable;
import java.util.List;
import tg.univlome.epl.entreprise.api.entities.Employe;
import tg.univlome.epl.entreprise.api.services.EmployeServiceBeanLocal;

/**
 *
 * @author kevo
 */
@Named
@RequestScoped
public class EmployeBean implements Serializable{
    
     @EJB
    private EmployeServiceBeanLocal employeService;
    private Employe employe;
    private List<Employe> listeEmployes;

    public void init() {
        this.employe = new Employe();
        this.listeEmployes = employeService.lister();
    }

    public String enregistrer() {
        employeService.ajouter(this.employe);
        this.employe = new Employe();
        return "listeEmployes?faces-redirect=true";
    }

    public String modifier(Employe emp) {
        employeService.modifier(emp);
        return "listeEmployes?faces-redirect=true";
    }

    public void supprimer(int id) {
        employeService.supprimer(id);
        this.listeEmployes = employeService.lister();
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public List<Employe> getListeEmployes() {
        return listeEmployes;
    }

    public void setListeEmployes(List<Employe> listeEmployes) {
        this.listeEmployes = listeEmployes;
    }
    
}
