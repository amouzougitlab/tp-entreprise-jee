/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.epl.entreprise.service;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import java.util.List;
import tg.univlome.epl.entreprise.api.entities.Departement;
import tg.univlome.epl.entreprise.api.services.DepartementServiceBeanLocal;

/**
 *
 * @author bouctou
 */
@Stateless
public class DepartementServiceBean implements DepartementServiceBeanLocal{
    
        @PersistenceContext(name = "entreprisePU")
    private EntityManager em;

    @Override
    public void ajouter(Departement departement) {
        em.persist(departement);
    }

    @Override
    public void modifier(Departement departement) {
        em.merge(departement);
    }

    @Override
    public List<Departement> lister() {
        Query query = em.createQuery("SELECT d FROM Departement d ORDER BY d.libelle");
        return query.getResultList();
    }

    @Override
    public void supprimer(int id) {
        Departement departement = em.find(Departement.class, id);
        if (departement != null) {
            em.remove(departement);
        }
    }

    @Override
    public Integer compter() {
        Query query = em.createQuery("SELECT COUNT(d) FROM Departement d");
        Long count = (Long) query.getSingleResult();
        return count.intValue();  
    }

    @Override
    public Departement recuperer(int id) {
        return em.find(Departement.class, id);
    }
    
}
