/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.epl.entreprise.web;

import jakarta.ejb.EJB;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Named;
import java.io.Serializable;
import java.util.List;
import tg.univlome.epl.entreprise.api.entities.Departement;
import tg.univlome.epl.entreprise.api.services.DepartementServiceBeanLocal;

/**
 *
 * @author bouctou
 */
@Named
@RequestScoped
public class DepartementBean implements Serializable{
    
        @EJB
    private DepartementServiceBeanLocal departementService;
    private Departement departement;
    private List<Departement> listeDepartements;

    public void init() {
        this.departement = new Departement();
        this.listeDepartements = departementService.lister();
    }

    public String enregistrer() {
        departementService.ajouter(this.departement);
        this.departement = new Departement();
        return "listeDepartements?faces-redirect=true";
    }

    public String modifier(Departement dep) {
        departementService.modifier(dep);
        return "listeDepartements?faces-redirect=true";
    }

    public void supprimer(int id) {
        departementService.supprimer(id);
        this.listeDepartements = departementService.lister();
    }

    public Departement getDepartement() {
        return departement;
    }

    public void setDepartement(Departement departement) {
        this.departement = departement;
    }

    public List<Departement> getListeDepartements() {
        return listeDepartements;
    }

    public void setListeDepartements(List<Departement> listeDepartements) {
        this.listeDepartements = listeDepartements;
    }
    
}
