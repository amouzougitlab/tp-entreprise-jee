/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.epl.entreprise.api.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author kevo
 */
@Entity
@Table(name = "departement")
public class Departement implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "libelle", nullable = false)
    private String libelle;

    @OneToOne 
    @JoinColumn(name = "responsable_id", nullable = true) // Peut être nullable si le département n'a pas de responsable
    private Employe responsable;

    @OneToMany(mappedBy = "parent")
    private Set<Departement> sousDepartements = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "parent_id", nullable = true) // Peut être nullable si le département n'a pas de parent (est un département principal)
    private Departement parent;

    @OneToMany(mappedBy = "departement")
    private Set<Employe> employes = new HashSet<>();


    public Departement() {
    }

    public Departement(Integer id, String libelle, Employe responsable, Departement parent) {
        this.id = id;
        this.libelle = libelle;
        this.responsable = responsable;
        this.parent = parent;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Employe getResponsable() {
        return responsable;
    }

    public Set<Departement> getSousDepartements() {
        return sousDepartements;
    }

    public void setSousDepartements(Set<Departement> sousDepartements) {
        this.sousDepartements = sousDepartements;
    }

    public Departement getParent() {
        return parent;
    }

    public void setParent(Departement parent) {
        this.parent = parent;
    }

    public Set<Employe> getEmployes() {
        return employes;
    }

    public void setEmployes(Set<Employe> employes) {
        this.employes = employes;
    }



    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.libelle);
        hash = 97 * hash + Objects.hashCode(this.responsable);
        hash = 97 * hash + Objects.hashCode(this.sousDepartements);
        hash = 97 * hash + Objects.hashCode(this.parent);
        hash = 97 * hash + Objects.hashCode(this.employes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Departement other = (Departement) obj;
        if (!Objects.equals(this.libelle, other.libelle)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.responsable, other.responsable)) {
            return false;
        }
        if (!Objects.equals(this.sousDepartements, other.sousDepartements)) {
            return false;
        }
        if (!Objects.equals(this.parent, other.parent)) {
            return false;
        }
        return Objects.equals(this.employes, other.employes);
    }

    @Override
    public String toString() {
        return "Departement{" + "id=" + id + ", libelle=" + libelle + ", responsable=" + responsable + ", sousDepartements=" + sousDepartements + ", parent=" + parent + ", employes=" + employes + '}';
    }

}
