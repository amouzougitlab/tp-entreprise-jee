/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package tg.univlome.epl.entreprise.api.services;

import jakarta.ejb.Local;
import java.util.List;
import tg.univlome.epl.entreprise.api.entities.Departement;

/**
 *
 * @author bouctou
 */
@Local
public interface DepartementServiceBeanLocal {

    void ajouter(Departement departement);

    void modifier(Departement departement);

    List<Departement> lister();

    void supprimer(int id);

    Integer compter();

    Departement recuperer(int id);

}
