/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package tg.univlome.epl.entreprise.api.services;

import jakarta.ejb.Local;
import java.util.List;
import tg.univlome.epl.entreprise.api.entities.Employe;

/**
 *
 * @author bouctou
 */
@Local
public interface EmployeServiceBeanLocal {

    void ajouter(Employe employe);

    void modifier(Employe employe);

    List<Employe> lister();

    void supprimer(int id);

    Integer compter();

    Employe recuperer(int id);
}
